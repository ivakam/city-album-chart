# SuperKayo

This repo runs off of Rails 5.2.0 as well as Ruby 2.4.4 (x64).

## Build instructions

Install ruby-2.4.4 via [rvm](https://rvm.io/)

Install dependencies:
```bash
bundle install
```

Set up the database:
```bash
rake db:setup
```

Run the server:
```bash
rails server
```