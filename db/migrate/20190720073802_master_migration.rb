class MasterMigration < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.string :title
      t.string :romanization
      t.string :romaji_artist
      t.string :japanese_artist
      t.string :year
      t.text :description
      t.text :coverlink
      t.text :thumbnail
      t.string :flavor
      t.string :tracklist
      t.string :temp_tracklist
      t.string :image
      t.text :tags
      t.integer :quality
      t.integer :user_id

      t.timestamps
    end
    create_table :tracks do |t|
      t.belongs_to :album, index: true
      t.string :title
      t.string :romanization
      t.string :duration
      t.integer :order

      t.timestamps
    end

    create_table :active_storage_blobs do |t|
      t.string   :key,        null: false
      t.string   :filename,   null: false
      t.string   :content_type
      t.text     :metadata
      t.bigint   :byte_size,  null: false
      t.string   :checksum,   null: false
      t.datetime :created_at, null: false

      t.index [ :key ], unique: true
    end

    create_table :active_storage_attachments do |t|
      t.string     :name,     null: false
      t.references :record,   null: false, polymorphic: true, index: false
      t.references :blob,     null: false

      t.datetime :created_at, null: false

      t.index [ :record_type, :record_id, :name, :blob_id ], name: "index_active_storage_attachments_uniqueness", unique: true
    end
    create_table :users do |t|
      t.string :username, unique: true
      t.string :email, unique: true
      t.string :password_digest
      t.boolean :admin
      t.boolean :banned
      t.string :gender
      t.integer :birth_year
      t.string :location
      t.string :bio
      t.boolean :email_confirmed, :default => false
      t.string :confirm_token
      t.boolean :password_token_expired, :default => false
      t.string :reset_password_token

      t.timestamps
    end
    create_table :reports do |t|
      t.string :album
      t.string :reason
      t.string :comment
      t.string :report_type
      t.integer :user_id
      t.string :target_id

      t.timestamps
    end
    create_table :forum_threads do |t|
      t.string :title
      t.string :category
      t.boolean :stickied
      t.boolean :archived
      t.boolean :locked
      t.text :body
      t.timestamps
    end

    create_table :posts do |t|
      t.belongs_to :forum_thread, index: true
      t.text :body
      t.timestamps
    end
    add_reference :forum_threads, :user, foreign_key: true
    add_reference :posts, :user, foreign_key: true
    add_column :users, :badges, :string
    add_column :users, :account_type, :string
    add_column :users, :signature, :string
    add_column :users, :album_fav, :string
    add_column :posts, :post_index, :integer
    create_table :upvotes do |t|
      t.belongs_to :user
      t.belongs_to :post
      t.belongs_to :comment
      t.belongs_to :article
      t.string :upvote_type
      t.bigint :target_id
      t.timestamps
    end
    create_table :articles do |t|
      t.string :title
      t.text :body
      t.belongs_to :user
      t.boolean :featured
      t.string :category
      t.string :subtitle
      t.timestamps
    end
    create_table :comments do |t|
      t.text :body
      t.belongs_to :user
      t.boolean :pinned
      t.belongs_to :article
      t.timestamps
    end
    create_table :notifications do |t|
      t.belongs_to :user
      t.string :notification_type
      t.bigint :target_id
      t.timestamps
    end
    create_table :subscriptions do |t|
      t.belongs_to :user
      t.string :subscription_type
      t.bigint :target_id
      t.timestamps
    end
    add_column :notifications, :unread, :boolean, :default => true
    create_table :announcements do |t|
      t.belongs_to :user
      t.string :title
      t.text :body
      t.timestamps
    end
    create_table :preferences do |t|
      t.belongs_to :user
      t.string :theme, :default => 'Kayo'
      t.string :date_format, :default => 'YYYY-MM-DD'
      t.boolean :autosubscribe, :default => true
      t.boolean :browser_notifications, :default => false
      t.timestamps
    end
  end
end
