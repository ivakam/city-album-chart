require 'faker'
require 'yaml'
Faker::UniqueGenerator.clear

def orphan_dummy
	user = User.new
	user.username = '<Deleted>'
	user.email = 'deleted@dummy.com'
	user.password = Faker::Internet.password(8, 16)
	user.admin = false
	user.banned = false
	user.gender = 'Unknown'
	user.birth_year = 'Unknown'
	user.location = ''
	user.bio = ''
	user.signature = ''
	user.badges = ''
	user.album_fav = ''
	user.save
	p "Dummy generated"
end

orphan_dummy

(0..50).each do | i |
	user = User.new
	user.username = Faker::Internet.unique.username
	user.email = Faker::Internet.unique.email
	user.password = Faker::Internet.password(8, 16)
	user.admin = rand(0..50) > 47 ? true : false
	user.banned = false
	user.gender = rand(0..1) == 1 ? 'Male' : 'Female'
	user.birth_year = rand(1900..2019)
	user.location = Faker::Address.country
	user.bio = Faker::MostInterestingManInTheWorld.quote
	user.signature = Faker::Dota.quote
	badge_arr = ['Admin ', 'VIP ', 'Honorary ', 'Funk_master ', 'Oldie ']
	user.badges = ''
	badge_arr.each do | b |
		if rand(0..2) == 1
			user.badges << b
		end
	end
	user.album_fav = ''
	albums = Album.all.size
	(0...5).each do | j |
		user.album_fav << rand(1..albums).to_s + ' '
	end
	user.save
	p "User #{i} generated"
    preference = Preference.new
    preference.user = user
    preference.save
    p "Preferences for user #{i} generated"
end

@usercount = User.all.length

(0..5).each do | i |
	thread = ForumThread.new
	thread.title = Faker::Book.title
	thread.category = 'rules'
	thread.stickied = true
	thread.archived = false
	thread.locked = false
	thread.user = User.find(rand(2..@usercount))
	thread.body = Faker::HitchhikersGuideToTheGalaxy.quote
	thread.save
	p "Thread #{i} generated"
end

(0..20).each do | i |
	thread = ForumThread.new
	thread.title = Faker::Book.title
	thread.category = 'rules'
	thread.stickied = false
	thread.archived = false
	thread.locked = false
	thread.user = User.find(rand(2..@usercount))
	thread.body = '<p>' + Faker::HitchhikersGuideToTheGalaxy.quote + '</p>'
	thread.save
	p "Thread #{i} generated"
end

ForumThread.all.each_with_index do | parentThread, i |
	(1..rand(5..10)).each do | j |
		reply = Post.new
		reply.user = User.find(rand(2..@usercount))
		reply.forum_thread = parentThread
		reply.body = '<p>' + Faker::HitchhikersGuideToTheGalaxy.quote + '</p>'
		reply.post_index = j
		(1..rand(5..20)).each do | k |
			upvote = Upvote.new
			upvote.target_id = reply.id
			upvote.upvote_type = 'Post'
			upvote.user = User.find(rand(2..@usercount))
			upvote.save
		end
		p "Post #{j} for thread ##{parentThread.id} #{parentThread.title} generated"
		reply.save
	end
end

(0..10).each do | i |
	article = Article.new
	article.user = User.find(rand(2..@usercount))
	article.title = Faker::Book.title
	article.subtitle = Faker::Lorem.sentence(4, false, 5)
	article.body = Faker::Lorem.paragraph(15)
	article.body << '\n ' + Faker::Lorem.paragraph(15)
	article.body << '\n ' + Faker::Lorem.paragraph(15)
	article.body << '\n ' + Faker::Lorem.paragraph(15)
	article.body << '\n ' + Faker::Lorem.paragraph(15)
	article.featured = i == 1 ? true : false
    article_banner =  Dir.glob(Rails.root.join("app/assets/images/bg/kayo/*.*"))[rand(0...Dir.glob(Rails.root.join("app/assets/images/bg/kayo/*.*")).size)]
	p article_banner
	article.banner.attach(io: File.open(article_banner), filename: article_banner.split(/\/bg\/kayo\//)[1])
	categories = ['review', 'essay', 'opinion_piece', 'history']
	article.category = categories[rand(0..3)]
	article.save
	(1..rand(5..20)).each do | k |
		upvote = Upvote.new
		upvote.upvote_type = 'Article'
		upvote.target_id = article.id
		upvote.user = User.find(rand(2..@usercount))
		upvote.save
	end
	p "Article #{i} generated"
end

Article.all.each_with_index do | article, i |
	(1..rand(5..20)).each do | j |
		reply = Comment.new
		reply.user = User.find(rand(2..@usercount))
		reply.article = article
		reply.body = Faker::HitchhikersGuideToTheGalaxy.quote
		(1..rand(5..20)).each do | k |
			upvote = Upvote.new
			upvote.upvote_type = 'Comment'
			upvote.target_id = reply.id
			upvote.user = User.find(rand(2..@usercount))
			upvote.save
		end
		p "Comment #{j} for article ##{article.id} #{article.title} generated"
		reply.save
	end
end


def CreateAlbumWithTracks(albumParam, tracks = [])
	current_album = Album.new(albumParam)
	cover_name = current_album.title.downcase.gsub(/[^[\u3000-\u303F][\u3040-\u309F][\u30A0-\u30FF][\uFF00-\uFFEF][\u4E00-\u9FAF][\u2605-\u2606][\u2190-\u2195]\u203B\p{L}\d\\]/, '')
	cover_path = Dir.glob(Rails.root.join("app/assets/images/#{cover_name}.*")).first
	p "Album Title: " + current_album.title
	p "Cover name: " + cover_name
	if cover_path == nil
		cover_path = Rails.root.join("app/assets/images/missingcover.jpg")
		current_album.cover.attach(io: File.open(cover_path), filename: "missingcover.jpg")
		p "ERROR: Could not find cover for #{current_album.title}"
	else
		current_album.cover.attach(io: File.open(cover_path), filename: cover_path[/(#{cover_name})\..+$/, 0])
	end
	p "Cover path: " + cover_path.to_s
    current_album.tags = "#{albumParam[:title]} #{albumParam[:romanization]} #{albumParam[:romaji_artist]} #{albumParam[:japanese_artist]} #{albumParam[:year]} #{albumParam[:description]} #{albumParam[:flavor].gsub(/,/,'')}"
    if albumParam[:user_id].present?
    	current_album.user_id = albumParam[:user_id]
    else
    	current_album.user_id = 1
    	#current_album.user_id = rand(2..User.all.size)
    end
    tracks.each do |t|
        current_album.tags << " #{t[:title]} #{t[:romanization]}"
    end
	temp_quality = 0
	track_duration_count = 0
	has_tracks = (tracks == []) ? false : true
	tracks.each_with_index do | t, i |
		album_track = Track.new
		album_track.title = t[:title]
		album_track.romanization = t[:romanization]
		album_track.duration = t[:duration]
		album_track.album = current_album
		album_track.order = i + 1
		album_track.save
		if t[:duration].present?
			track_duration_count += 1
		end
	end
	if current_album.description.present?
		temp_quality += 5
	end
	if current_album.year.present?
		temp_quality += 10
	end
	if current_album.flavor.present?
		temp_quality += 5
	end
	if has_tracks
		temp_quality += 30
	end
	if track_duration_count == tracks.length
		temp_quality += 5
	end
	if current_album.coverlink.present?
		temp_quality += 10
	end
	current_album.quality = temp_quality
	current_album.save
	p "ID: " + current_album.id.to_s
end

@album_data = YAML::load(File.open('albums.yml', 'r'))
@album_data.each do | key, value |
	CreateAlbumWithTracks(value[:album_info], value[:tracks])
end
