$(document).on 'turbolinks:load', ->
    backgrounds = $("#albums-bg-container").children()
    chosenAlbum = 0
    backgrounds.removeClass('selected')
    backgrounds.first().addClass('selected')
    window.setInterval( ->
        if chosenAlbum < backgrounds.length
            chosenAlbum++
        else
            chosenAlbum = 1
        backgrounds.each ->
            $(this).removeClass('selected')
        $(backgrounds[chosenAlbum]).addClass('selected')
    ,120000)
