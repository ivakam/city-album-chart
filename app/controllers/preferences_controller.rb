class PreferencesController < ApplicationController
    def update
        pref = Preference.find_by(user: get_user)
        autosubscribe = (params[:preference][:autosubscribe].present? && params[:preference][:autosubscribe] == 'true') ? true : false
        pref.update_attribute('theme', params[:preference][:theme])
        pref.update_attribute('date_format', params[:preference][:date_format])
        pref.update_attribute('autosubscribe', autosubscribe)
        pref.update_attribute('browser_notifications', false)
        redirect_to request.referrer, notice: 'Preferences successfully updated!'
    end
end
