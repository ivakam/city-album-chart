module Recaptcha
    RECAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify".freeze

    def validate_recaptcha(threshold = 0.3)
        # params.require raises an exception if recaptcha_token is missing
        token = params.require(:recaptcha_token)
        host_file = File.read(File.expand_path(Rails.root + 'host_path.json'))
        result = JSON.parse(
            # using RestClient to keep things clean and simple
            RestClient.post(RECAPTCHA_URL,
                            :secret => JSON.parse(host_file)['captcha_key'],
                            :response => token,
                            :remoteip => request.remote_ip)
        )
        # if the Google API results aren't a success, we return false
        return false unless result["success"]
        # if the score is below our threshold, we also return false
        return false if result["score"] < threshold
        # otherwise, the request was validated and is above our threshold
        return true
    rescue ActionController::ParameterMissing
        return false
    rescue RestClient::Exceptions::Timeout
        # you can decide if you want to fail-open or fail-close here.
        return true
    end
end
