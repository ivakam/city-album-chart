require 'json'
require 'base64'

class AlbumsController < ApplicationController
	def contains_cjk?
		!!(self =~ /\p{Han}|\p{Katakana}|\p{Hiragana}|\p{Hangul}/)
	end
	
	def t_pop(albums, params)
		if !albums.any?
			render :json => ["Out of albums to render!"]
			return
		end
		album_ids = ""
		albums.each do | album |
			album_ids << "'#{album["id"].to_s}', "
		end
		album_ids = album_ids.gsub(/,\s+$/, '')
		tracks = Track.find_by_sql("SELECT * FROM tracks WHERE album_id IN (#{album_ids})")
		tracks = tracks.sort_by(&:order)
		album_result = []
		albums.each_with_index do | album, index |
			album_with_tracks = {}
			album.attributes.each_pair do | key, value |
				album_with_tracks[key] = value
			end
			album_with_tracks['contributor'] =  (!album.user_id.nil?) ? User.find_by(id: album.user_id).username : 'Unknown'
			album_with_tracks['upload_date'] = album.created_at.strftime("%-b %-d %Y")
			album_with_tracks['thumbnail'] = album.cover.variant(resize: '200x200').processed.service_url
			album_with_tracks['coverlink'] = album.cover.service_url
			temp_tracks = []
			tracks.each do | track |
				if track["album_id"] == album["id"]
					temp_tracks.push(track)
				end
			end
			album_with_tracks["tracks"] = temp_tracks
			album_result[index] = album_with_tracks
		end
		render :json => album_result
	end
	
	def show
		@albums = Album.all.order('created_at desc').limit(40)
		@albums.each do | a |
			a.thumbnail = a.cover.variant(resize: '200x200').processed.service_url
			a.coverlink = a.cover.service_url
		end
	end
	
	def fetch
		@albums = Album.all
		if params.to_unsafe_hash.size === 2
			@albums = t_pop(@albums, params)
			return
		end
		if params[:q] == nil || params[:q] == ''
			params.each do | key, value |
				if key == "title" || key == "romaji_artist" || key == "japanese_artist" || key == "year" || key == "flavor"
					@albums = Album.where("#{key} LIKE ?", '%' + value + '%')
				end
			end
		else
            querystr = "tags LIKE '%#{params[:q].split.empty? ? '' : params[:q].split[0] }%'"
            params[:q].split[1..-1].to_a.each do |q|
                querystr << " AND tags LIKE '%#{q}%'"
            end
            @albums = Album.where(querystr)
		end
		if params[:sort] != nil
			params[:sort].downcase!
		end
		if params[:sort] == "asc" || params[:sort] == "desc"
			sort_type = params[:sort_type].downcase
			if sort_type == "artist"
				sort_type = "romaji_artist"
			end
			if params[:sort] == "asc"
				@albums = @albums.order(Arel.sql(sort_type.downcase))
			end
			if params[:sort] == "desc"
				@albums = @albums.order(Arel.sql(sort_type.downcase)).reverse_order
			end
		else
			@albums = @albums.order('created_at desc').reverse_order
		end
		if params[:limit]
			@albums = @albums.limit(params[:limit])
		end
		if params[:offset]
			@albums = @albums.offset(params[:offset])
		end
		@albums = t_pop(@albums, params)
	end
	
	def create
		if get_user
            if get_user.email_confirmed
				if params[:scraper].present?
					scraper_albums = params[:scraper]
					scraper_albums.each do | a |
						a = a[1]
						album = Album.new()
						album.flavor = a[".flavor"]
						album.description = a[".description"]
						album.romanization = a[".romanization"]
						album.title = a[".title"]
						album.japanese_artist = a[".japanese_artist"]
						album.romaji_artist = a[".romaji_artist"]
						album.year = a[".year"]
						album.user_id = get_user.id
						album.tags = "#{album.title} #{album.romaji_artist} #{album.year}"
						album.quality = album.year.present? ? 45 : 35
						cover_path = Rails.root.join("app/assets/images/missingcover.jpg")
						if a[".cover"].present?
							album.cover.attach(a[".cover"])
						elsif a[".cover_base64"].present?
							regex = /\Adata:image\/(.+)?;base64,(.*)/m
							data_uri_parts = a[".cover_base64"].match(regex) || []
							extension = data_uri_parts[1]
							file_name = "temp_cover.#{extension}"
							File.open(file_name, 'wb') do | f |
								f.write(Base64.decode64(data_uri_parts[2]))
							end
							album.cover.attach(io: File.open(file_name), filename: file_name)
							File.delete(file_name) if File.exists?(file_name)
						else
							album.cover.attach(io: File.open(cover_path), filename: "missingcover.jpg")
						end
						if !album.save
							album.cover.purge
							album.cover.attach(io: File.open(cover_path), filename: "missingcover.jpg")
							flash[:notice] = album.errors[:base][0]
							flash.keep(:notice)
							return
						end
						a[".tracklist"].each do | t |
							t = t[1]
							track = Track.new()
							track.title = t[".title"]
							track.romanization = t[".romanization"]
							track.duration = t[".duration"]
							track.album = album
							track.order = t[".order"]
							album.tags << " #{track.title}"
							track.save
						end
					end
					flash[:notice] = 'Album submitted!'
					flash.keep(:notice)
					render js: "location.reload()"
				elsif params[:album][:title].present? && params[:album][:romaji_artist].present?
					@album = Album.new(album_params)
					@album.tags = "#{params[:album][:title]} #{params[:album][:romanization]} #{params[:album][:romaji_artist]} #{params[:album][:japanese_artist]} #{params[:album][:year]} #{params[:album][:description]} #{params[:album][:flavor].gsub(/,/,'')}"
					temp_quality = 0
					if params[:tracklist] != ''
							parsed_tracks = JSON.parse(params[:tracklist])
						parsed_tracks.each do | t |
							@album.tags << " #{t[1][:romanization]} #{t[1][:title]}"
						end
						track_duration_count = 0
						parsed_tracks.each_with_index do | t, i |
						t[1][:order] = i + 1
						end
						parsed_tracks.each do | t |
							if t[1]['duration'].present?
								track_duration_count += 1
							end
						end
						@album.tracks = parsed_tracks.map { | t | Track.new(t[1])}
					else
						parsed_tracks = []
					end
					has_tracks = parsed_tracks.empty?
					if  @album.description.present?
					    temp_quality += 5
					end
					if  @album.year.present?
					    temp_quality += 10
					end
					if  @album.flavor.present?
					    temp_quality += 5
					end
					if has_tracks
					    temp_quality += 30
					end
					if track_duration_count == parsed_tracks.length
					    temp_quality += 5
					end
					if @album.cover.present?
					    temp_quality += 10
					end
					@album.quality = temp_quality
					@album.user_id = get_user.id
					if @album.save
						redirect_to '/albums/submit', notice: 'Album submitted!'
					else
						redirect_to '/albums/submit', notice: @album.errors[:base][0]
					end
				else
					p 'Invalid parameters for album'
				end
            else
                activation_barrier
            end
		else
			login_barrier
		end
	end
	
	def update
		if get_user && get_user.email_confirmed
			param_album = params[:album]
			param_tracks = JSON.parse(params[:tracklist])
			@album = Album.find_by(title: param_album[:title_old], romaji_artist: param_album[:romaji_artist_old])
			@album.update(
				title: param_album[:title],
				romanization: param_album[:romanization],
				japanese_artist: param_album[:japanese_artist],
				romaji_artist: param_album[:romaji_artist],
				year: param_album[:year],
				flavor: param_album[:flavor],
				description: param_album[:description],
				tags: "#{params[:album][:title]} #{params[:album][:romanization]} #{params[:album][:romaji_artist]} #{params[:album][:japanese_artist]} #{params[:album][:year]} #{params[:album][:description]} #{params[:album][:flavor].gsub(/,/,'')}"
			)
			delete_list = params[:delete_list].split(/\+\+\+/)
			delete_list.each do | track |
				to_be_nuked = Track.find_by(album_id: @album.id, title: track)
				to_be_nuked.destroy
			end
			param_tracks.each_with_index do | track, i |
				track['order'] = i + 1
				real_track = Track.find_by(album_id: @album.id, title: track['title_old'])
				if real_track.nil?
					temp_track = Track.new(title: track['title'], romanization: track['romanization'], duration: track['duration'], order: track['order'])
					temp_track.album = @album
					temp_track.save
				else
					real_track.update(
						title: track['title'],
						romanization: track['romanization'],
						duration: track['duration'],
						order: track['order']
					)
				end
			end
			@album.tracks.each do | t |
				@album.tags << " #{t.title} #{t.romanization}"
			end
			if param_album[:cover].present?
				if @album.cover.attached?
					@album.cover.purge
					@album.cover.attach(param_album[:cover])
				else
					@album.cover.attach(param_album[:cover])
				end
				if !@album.save
					cover_path = Rails.root.join("app/assets/images/missingcover.jpg")
					@album.cover.purge
					@album.cover.attach(io: File.open(cover_path), filename: "missingcover.jpg")
					redirect_to request.referrer, notice: @album.errors[:base][0]
					return
				end
			end
			redirect_to request.referrer, notice: "Album updated!"
		else
			on_access_denied
		end
	end
	
	def destroy
		if get_user && get_user.admin
			to_be_nuked = JSON.parse(params[:album][:serialized_ids])
			to_be_nuked.each do | album |
				@album = Album.find_by(id: album)
				@album.destroy
			end
		else
			on_access_denied
		end
	end
	
	def submit
        if !get_user
            login_barrier
        elsif get_user && !get_user.email_confirmed
            activation_barrier
        else
		    render 'submit'
        end
	end
	
	private 
	
	def album_params
		params.require(:album).permit(:description, :romanization, :duration, :image, :title, :romaji_artist, :japanese_artist, :flavor, :year, :cover)
	end
end
