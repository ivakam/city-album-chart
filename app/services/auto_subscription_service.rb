class AutoSubscriptionService
    def initialize(params)
        @current_user = params[:model].user
        @target_id = params[:model].id
        @subscription_type = params[:model].class.name
    end
    
    def auto_subscribe
        if @current_user.preference.autosubscribe
            if !Subscription.where(user: @current_user, target_id: @target_id, subscription_type: @subscription_type).present?
                @subscription = Subscription.new
                @subscription.user = @current_user
                @subscription.target_id = @target_id
                @subscription.subscription_type = @subscription_type
                @subscription.save
            end
        end
    end
end