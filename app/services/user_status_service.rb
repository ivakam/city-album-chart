class UserStatusService
    def initialize(params)
        @current_user = params[:user]
        @upvote = params[:upvote]
    end
    
    def update
        return
        if @upvote
            @current_user = @upvote.post.user
        end
        u = Upvote.where(user: @current_user).size
        po = Post.where(user: @current_user).size
        a = Album.where(user_id: @current_user.id).size
        type = 'Member'
        if u >= 10 || po >= 10
            type = 'Plastic lover'
            if u >= 50 && po >= 10
                type = 'City popper'
                if u >= 100 && po >= 50 && a >= 5
                    type = 'Dorothy\'s Sister'
                    if u >= 250 && po >= 100 && a >= 20
                        type = 'Flapper'
                        if u >= 500 && po >= 250 && a >= 25
                            type = 'Awooo'
                            if u >= 1000 && po >= 500 && a >= 40
                                type = 'Bannai Tarao'
                            end
                        end
                    end
                end
            end
        end
        if @current_user.username == 'PorousBoat'
            type = 'Hoshiimo Kozô'
        end
        if type != @current_user.account_type
            @current_user.update_attribute('account_type', type)
        end
    end
end