class UserMailer < ApplicationMailer
    default from: 'staff@superkayo.xyz'

    def email_confirmation
        @current_user = params[:user]
        mail(:to => "#{@current_user.username} <#{@current_user.email}>", :subject => "Email Confirmation")
    end

    def password_reset
        @current_user = params[:user]
        mail(:to => "#{@current_user.username} <#{@current_user.email}>", :subject => "Password Reset")
    end
end
